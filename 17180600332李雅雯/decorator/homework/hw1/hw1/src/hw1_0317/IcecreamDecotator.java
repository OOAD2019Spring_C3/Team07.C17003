package hw1_0317;

public abstract class IcecreamDecotator implements Icecream{
	Icecream Speciallceream;
	protected String name;
	
	public IcecreamDecotator(Icecream Speciallceream){
		this.Speciallceream = Speciallceream;
		this.name = this.Speciallceream.makeIcecream();
	}
	public abstract String makeIcecream();
}
