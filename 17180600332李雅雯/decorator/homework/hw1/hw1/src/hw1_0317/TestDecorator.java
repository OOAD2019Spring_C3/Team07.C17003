package hw1_0317;
import hw1_0317.Icecream;
import hw1_0317.SimpleIcecream;
import hw1_0317.HoneyDecorator;
import hw1_0317.NuttyDecorator;

public class TestDecorator {
	public Icecream one = new SimpleIcecream();
	public static void main(String[] args){
		TestDecorator example = new TestDecorator();
		
		NuttyDecorator NuttyIce = new NuttyDecorator(example.one);
		NuttyIce.addNutt();
		System.out.println(NuttyIce.makeIcecream());
		
		HoneyDecorator HoneyIce = new HoneyDecorator(example.one);
		HoneyIce.addHoney();
		System.out.println(HoneyIce.makeIcecream());
	}
}
